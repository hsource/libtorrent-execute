// builds all boost.asio SSL source as a separate compilation unit
#include <boost/version.hpp>

#if BOOST_VERSION >= 104610
// Moved to asio.cpp: this causes a linker error when using libboost-1.48
//#include <boost/asio/ssl/impl/src.hpp>
#endif

