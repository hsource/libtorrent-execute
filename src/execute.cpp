/*

Copyright (c) 2006, MassaRoddel, Arvid Norberg
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

* Redistributions of source code must retain the above copyright
notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in
the documentation and/or other materials provided with the distribution.
* Neither the name of the author nor the names of its
contributors may be used to endorse or promote products derived
from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*/

#include "libtorrent/pch.hpp"

#ifndef TORRENT_DISABLE_EXTENSIONS

#ifdef _MSC_VER
#pragma warning(push, 1)
#endif

#include <ctime>
#include <fstream>
#include <iostream>

#include <boost/filesystem.hpp>
#include <boost/iostreams/stream.hpp>
#include <boost/process.hpp>
#include <boost/random/uniform_int_distribution.hpp>
#include <boost/shared_ptr.hpp>

#ifdef _MSC_VER
#pragma warning(pop)
#endif

#include "libtorrent/peer_connection.hpp"
#include "libtorrent/bt_peer_connection.hpp"
#include "libtorrent/bencode.hpp"
#include "libtorrent/torrent.hpp"
#include "libtorrent/extensions.hpp"
#include "libtorrent/broadcast_socket.hpp"
#include "libtorrent/socket_io.hpp"
#include "libtorrent/peer_info.hpp"
#include "libtorrent/random.hpp"

#include "libtorrent/extensions/execute.hpp"

#ifdef TORRENT_VERBOSE_LOGGING
#include "libtorrent/lazy_entry.hpp"
#endif

namespace libtorrent {
	namespace
	{
		const char extension_name[] = "hy_execute";
		const int extension_index = 9; // See bt_peer_connection.hpp

		enum {
			new_request,
			full_response
		};

		enum {
			ok = 200,
			incomplete = 600,
			not_executable,
			binary_not_found,
		};

		struct incoming_request {
			lazy_entry const& incoming_msg;
		};
	}

	boost::shared_ptr<execute_peer_plugin> execute_plugin::find_peer_plugin() {
		boost::shared_ptr<execute_peer_plugin> peer_plugin;
		for (std::list<boost::weak_ptr<execute_peer_plugin> >::iterator i = m_peer_plugins.begin();
			!m_peer_plugins.empty() && i != m_peer_plugins.end() && !peer_plugin;) {
			peer_plugin = i->lock();
			if (!peer_plugin)
				i = m_peer_plugins.erase(i);
			else
				++i;
		}

		return peer_plugin;
	}

	bool execute_plugin::execute(entry const& body, execute_callback_type callback)
	{
		// Try to find a peer supporting execution
		boost::shared_ptr<execute_peer_plugin> peer_plugin = find_peer_plugin();

		// If we don't have a connection, try to connect to every peer until we
		// find one that has the plugin
		if (!peer_plugin) {
			// Debug
			std::cout << "Peers: " << m_torrent.num_peers() << std::endl;
			for (int i = 0; i < m_torrent.num_peers() && !peer_plugin; i++) {
				bool peer_connected = m_torrent.try_connect_peer();
				if (peer_connected) {
					peer_plugin = find_peer_plugin();
				}
			}
		}

		if (!peer_plugin) {
			return false;
		}

		// Try to execute the outgoing body using the peer plugin
		peer_plugin->execute(body, callback);
		return true;
	}

	boost::shared_ptr<peer_plugin> execute_plugin::new_connection(peer_connection* pc)
	{
		if (pc->type() != peer_connection::bittorrent_connection)
			return boost::shared_ptr<peer_plugin>();

		boost::shared_ptr<execute_peer_plugin> pp = boost::shared_ptr<execute_peer_plugin>(
			new execute_peer_plugin(m_torrent, *pc, *this));

		m_peer_plugins.push_back(boost::weak_ptr<execute_peer_plugin>(pp));
		return pp;
	}

	execute_peer_plugin::execute_peer_plugin(torrent& t, peer_connection& pc, execute_plugin& tp)
		: m_torrent(t)
		, m_pc(pc)
		, m_tp(tp)
		, m_peer_extension_index(0)
		, m_rng(std::time(0))
	{
	}

	void execute_peer_plugin::add_handshake(entry& h)
	{
		entry& messages = h["m"];
		messages[extension_name] = extension_index; // This can be anything, as long as it's true
	}

	bool execute_peer_plugin::on_extension_handshake(lazy_entry const& h)
	{
		m_peer_extension_index = 0;
		if (h.type() != lazy_entry::dict_t) return false;
		lazy_entry const* messages = h.dict_find("m");
		if (!messages || messages->type() != lazy_entry::dict_t) return false;

		m_peer_extension_index = int(messages->dict_find_int_value(extension_name, -1));
		if (m_peer_extension_index == -1) return false;

		return true;
	}

	bool execute_peer_plugin::execute(entry const& body, execute_callback_type callback)
	{
		entry request;

		// Generate a random unique request ID
		boost::random::uniform_int_distribution<size_type> dist(0, std::numeric_limits<size_type>::max());
		size_type request_id = dist(m_rng);
		request["msg_type"] = new_request;
		request["request_id"] = request_id;
		request["body"] = body;

		// Debug
		std::cout << "Sending message: " << std::endl;
		request.print(std::cout);

		send_message(request);
		m_unfinished_requests[request_id] = std::make_pair(body, callback);

		return true;
	}

	bool execute_peer_plugin::on_extended(int length, int msg, buffer::const_interval body)
	{
		std::cout << "Received extended message: " << msg << std::endl;
		std::cout << "Length: " << length << "; Left: " << body.left() << std::endl;
	
		if (msg != extension_index) return false;
		if (m_peer_extension_index == 0) return false;
		if (body.left() != length) return true; // Wait until we have the full message
		

		// Decode the body
		lazy_entry ex_msg;
		error_code ec;
		int ret = lazy_bdecode(body.begin, body.end, ex_msg, ec);

		if (ret != 0 || ex_msg.type() != lazy_entry::dict_t)
		{
			m_pc.disconnect(errors::invalid_execute_message, 2); // Don't know why it's 2 but I don't ask
			return true;
		}
		
		std::cout << "OK Decoding" << std::endl;

		// Check there's a proper request_id
		lazy_entry const* p = ex_msg.dict_find_int("request_id");
		if (!p) {
			return true;
		}
		
		std::cout << "Have request ID" << std::endl;

		// Find the message type
		p = ex_msg.dict_find_int("msg_type");

		if (p) {
			std::cout << "Received message of type: " << p->int_value() << std::endl;
		
			switch (p->int_value()) {
			case new_request:
				return on_new_request(ex_msg);
				break;
			case full_response:
				return on_full_response(ex_msg);
				break;
			}
		}

		return true;
	}

	std::vector<char> execute_peer_plugin::encode_entry(entry const& msg) {
		std::vector<char> encoded_msg;
		bencode(std::back_inserter(encoded_msg), msg);
		return encoded_msg;
	}

	void execute_peer_plugin::send_message(entry const& body) {
		std::vector<char> encoded_body = encode_entry(body);

		char msg[6];
		char* ptr = msg;
		detail::write_uint32(1 + 1 + encoded_body.size(), ptr);
		detail::write_uint8(bt_peer_connection::msg_extended, ptr);
		detail::write_uint8(m_peer_extension_index, ptr);
		m_pc.send_buffer(msg, sizeof(msg));
		m_pc.send_buffer(&encoded_body[0], encoded_body.size());
	}

	bool execute_peer_plugin::on_new_request(lazy_entry const& msg) {
		// Assume we can execute it

		entry response;
		response["msg_type"] = full_response;
		response["request_id"] = msg.dict_find_int_value("request_id", 0);
		
		std::cout << "Processing request" << std::endl;

		// If don't have the full file, can't run it at all
		if (!m_torrent.is_seed()) {
			response["code"] = incomplete;
			send_message(response);
			return true;
		}
		
		std::cout << "Is seed" << std::endl;
		
		// Check manifest exists
		std::string root_path = m_torrent.save_path() + "/" + m_torrent.name();
		std::cerr << root_path << std::endl; // TODO: remove

		std::string manifest_file = root_path + "/MANIFEST.torex";
		boost::filesystem::path manifest_path(manifest_file);
		if (!boost::filesystem::exists(manifest_path)) {
			response["code"] = not_executable;
			send_message(response);
			return true;
		}
		
		std::cout << "Manifest exists" << std::endl;

		// Read the manifest for the command to execute
		std::ifstream t(manifest_file.c_str());
		std::stringstream command_buffer;
		command_buffer << t.rdbuf();
		t.close();
		std::string command = command_buffer.str();
		size_t first_space = command.find_first_of(' ');

		std::string executable;
		if (first_space == std::string::npos) {
			executable = command;
		} else {
			executable = command.substr(0, first_space);
		}
	
		std::cout << "Command: " << command_buffer.str() << std::endl;

		namespace bp = boost::process;
		namespace bi = boost::process::initializers;
		namespace io = boost::iostreams;
		
		// Check executable exists
		executable = bp::search_path(executable);
		
		if (executable.empty()) {
			response["code"] = binary_not_found;
			send_message(response);
			return true;
		}

		// Execute!
		bp::pipe p = bp::create_pipe();
		io::file_descriptor_sink sink(p.sink, io::close_handle);

		bp::child c = bp::execute(bi::run_exe(executable)
			, bi::set_cmd_line(command_buffer.str())
			, bi::bind_stdout(sink)
			, bi::start_in_dir(root_path.c_str())
			, bi::inherit_env());

		sink.close();

		std::cout << "Executed" << std::endl;			
		bp::wait_for_exit(c);
		std::cout << "Exited" << std::endl;
			
		io::file_descriptor_source source(p.source, io::close_handle);
		io::stream<io::file_descriptor_source> is(source);

		std::string s;
		std::ostringstream output;
		while (is >> s) {
			output << s;
		}

		response["code"] = ok;
		response["body"] = output.str();
		
		std::cout << "Output: " << output.str() << std::endl;

		send_message(response);
		std::cout << "Sending message:" << std::endl;
		response.print(std::cout, 1);
		return true;
	}

	bool execute_peer_plugin::on_full_response(lazy_entry const& msg) {
		size_type request_id = msg.dict_find_int_value("request_id", 0);
		
		std::cout << "Request ID: " << request_id << std::endl;
		
		for (std::map<size_type, execute_request_info>::iterator i = m_unfinished_requests.begin();
			 i != m_unfinished_requests.end(); i++) {
			std::cout << "Saved RID: " << i->first << std::endl;
		}
	
		// Discard response if not to proper request
		if (m_unfinished_requests.find(request_id) == m_unfinished_requests.end()) {
			return true;
		}
		
		std::cout << "Request entry exists" << std::endl;

		// Call the callback
		execute_request_info& info = m_unfinished_requests[request_id];
		info.second(info.first, msg);

		m_unfinished_requests.erase(request_id);
		return true;
	}

}

namespace libtorrent
{
	boost::shared_ptr<torrent_plugin> create_execute_plugin(torrent* t, void*)
	{
		return boost::shared_ptr<torrent_plugin>(new execute_plugin(*t));
	}
}

#endif

