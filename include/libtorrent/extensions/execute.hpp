/*

Copyright (c) 2014, Harry Yu
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

* Redistributions of source code must retain the above copyright
notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in
the documentation and/or other materials provided with the distribution.
* Neither the name of the author nor the names of its
contributors may be used to endorse or promote products derived
from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef TORRENT_EXECUTE_HPP_INCLUDED
#define TORRENT_EXECUTE_HPP_INCLUDED

#ifdef _MSC_VER
#pragma warning(push, 1)
#endif

#include <boost/shared_ptr.hpp>
#include <boost/random/mersenne_twister.hpp>

#include "libtorrent/config.hpp"
#include "libtorrent/extensions.hpp"
#include "libtorrent/torrent.hpp"

#ifdef _MSC_VER
#pragma warning(pop)
#endif

namespace libtorrent
{
	class torrent;

	struct execute_peer_plugin;
	typedef boost::weak_ptr<execute_peer_plugin> peer_plugin_ptr;
	typedef boost::function<void(entry, lazy_entry const&)> execute_callback_type;
	typedef std::pair<entry, execute_callback_type> execute_request_info;

	struct execute_plugin : torrent_plugin
	{
		execute_plugin(torrent& t) : m_torrent(t) {}

		virtual char const* type() const { return "execute"; }
		virtual boost::shared_ptr<peer_plugin> new_connection(peer_connection* pc);
		bool execute(entry const& body, execute_callback_type callback);

	private:
		torrent& m_torrent;
		// Note: using a list means we by default try to access the first peer
		// whenever we want to execute something. This is OK for prototype but
		// not for real implementation
		std::list<boost::weak_ptr<execute_peer_plugin> > m_peer_plugins;

		boost::shared_ptr<execute_peer_plugin> find_peer_plugin();
	};

	struct execute_peer_plugin : peer_plugin
	{
		execute_peer_plugin(torrent& t, peer_connection& pc, execute_plugin& tp);

		virtual char const* type() const { return "execute"; }

		// adds a component to the extension handshake
		virtual void add_handshake(entry& h);

		virtual bool on_extension_handshake(lazy_entry const& h);
		virtual bool on_extended(int length, int msg, buffer::const_interval body);

		bool execute(entry const& body, execute_callback_type callback);

	private:
		torrent& m_torrent;
		peer_connection& m_pc;
		execute_plugin& m_tp;
		int m_peer_extension_index;
		boost::mt19937 m_rng;
		std::map<size_type, execute_request_info> m_unfinished_requests;

		std::vector<char> encode_entry(entry const& msg);
		void send_message(entry const& body);
		bool on_new_request(lazy_entry const& msg);
		bool on_full_response(lazy_entry const& msg);
	};

	boost::shared_ptr<torrent_plugin> create_execute_plugin(torrent*, void*);
}

#endif // TORRENT_EXECUTE_HPP_INCLUDED
