/*

Copyright (c) 2003, Arvid Norberg
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in
      the documentation and/or other materials provided with the distribution.
    * Neither the name of the author nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*/

#include <stdlib.h>
#include <time.h>
#include <iostream>
#include "libtorrent/entry.hpp"
#include "libtorrent/bencode.hpp"
#include "libtorrent/session.hpp"
#include "libtorrent/peer_info.hpp"
#include "libtorrent/torrent_handle.hpp"
#include "libtorrent/torrent_info.hpp"


using namespace libtorrent;


void print_debug(torrent_handle& h) {
	std::vector<peer_info> peer_info;
	h.get_peer_info(peer_info);
	std::vector<announce_entry> trackers = h.trackers();

	std::cout << "Num trackers: " << trackers.size() << "; Num peers: " << peer_info.size() << std::endl;
	for (int i = 0; i < trackers.size(); i++) {
		std::cout << "URL: " << trackers[i].url << "; Message: " << trackers[i].message
			<< "; Next announce: " << trackers[i].next_announce_in() << "; Fails: " << (int) trackers[i].fails
			<< "; Updating: " << trackers[i].updating << "; Verified: " << trackers[i].verified << std::endl;
	}

	for (int i = 0; i < peer_info.size(); i++) {
		std::cout << peer_info[i].ip << std::endl;
	}
}


void handle_response(entry request, lazy_entry const& response_lazy) {
	std::cout << "Request:" << std::endl;
	request.print(std::cout, 1);

	entry response;
	response = response_lazy;
	std::cout << "Response:" << std::endl;
	response.print(std::cout, 1);
	printf("%u\n\n\n", (unsigned) time(0)); 
}


int main(int argc, char* argv[])
{
	if (argc != 3)
	{
		fputs("usage: ./simple_client torrent-file y/n\n"
			"to stop the client, press return.\n", stderr);
		return 1;
	}

	// Start the torrent
	session s;
	session_settings settings = s.settings();
	settings.announce_to_all_trackers = true;
	settings.announce_to_all_tiers = true;
	settings.close_redundant_connections = false;
	s.set_settings(settings);
	error_code ec;
	s.listen_on(std::make_pair(6881, 6889), ec);
	if (ec)
	{
		fprintf(stderr, "failed to open listen socket: %s\n", ec.message().c_str());
		return 1;
	}

	add_torrent_params p;
	p.save_path = "./";
	p.ti = new torrent_info(argv[1], ec);
	if (ec)
	{
		fprintf(stderr, "%s\n", ec.message().c_str());
		return 1;
	}

	torrent_handle h = s.add_torrent(p, ec);
	if (ec)
	{
		fprintf(stderr, "%s\n", ec.message().c_str());
		return 1;
	}
	
	// If the user really just wants to execute
	if (argv[2] != 0 && argv[2][0] == 'y') {
		while (true) {
			print_debug(h);
			entry request;
			request["blank"] = 0;
			printf("%u\n\n\n", (unsigned) time(0)); 
			bool executed = h.request_execute(request, &handle_response);
			if (!executed) {
				std::cout << "Not executed" << std::endl;
			}
			else {
				std::cout << "Executed" << std::endl;
			}
			char a;
			scanf("%c\n", &a);
		}
	}

	else {
		while (true) {
			print_debug(h);
			char a;
			scanf("%c\n", &a);
		}
	}


	// wait for the user to end
	
	
	return 0;
}

