#!/bin/bash

pushd ~/libtorrent
git pull
#cp -f src/execute.cpp ../lt/src

cp -f src/*.cpp ../lt/src
cp -f include/libtorrent/*.hpp ../lt/include/libtorrent
cp -f include/libtorrent/extensions/*.hpp ../lt/include/libtorrent/extensions
#cp -f m4/* ../lt/m4
#cp -f src/Makefile.am ../lt/src
#cp -f include/libtorrent/Makefile.am ../lt/include/libtorrent
#cp -f configure.ac ../lt
popd
